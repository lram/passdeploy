# coding=utf-8
"""
This modules tests all web service plugins.

The current credentials (username and passphrase) are stored in the first
line of text files named ${WEB_SERVICE_NAME}.txt separated by a semicolon.
Backup of old credentials are stored in text files named
${WEB_SERVICE_NAME}.old-cred.txt .

Logs are stored in a text file named web-service-plugins-test.log

This module is configured manually. All tested web services have to be
entered into the services tuple.
"""

import logging
import string
import random
import sys
import passdeploy


def generate_passphrase(size=15, chars=string.ascii_letters + string.digits):
    """Generates and returns a passphrase of a given size and character set.

    WARNING: Note that this function is not using a secure pseudo random
    number generator. But for this purpose on testing passdeploy, it should
    be OK.

    :param size: Passphrase size.
    :param chars: Included character within the passphrase.
    :return: str -- generated passphrase.
    """

    return ''.join(random.choice(chars) for _ in range(size))


# setup logging to output on file and console

FORMAT = '%(asctime)s [%(name)s] %(levelname)s %(message)s'
DATEFMT = '%m/%d/%Y %I:%M:%S %p'

logging.basicConfig(
    level=logging.INFO,
    format=FORMAT,
    datefmt=DATEFMT
)

LOG_FORMATTER = logging.Formatter(
    fmt=FORMAT,
    datefmt=DATEFMT)

LOGGER = logging.getLogger()

FILE_HANDLER = logging.FileHandler("web-service-plugins-test.log")
FILE_HANDLER.setFormatter(LOG_FORMATTER)
LOGGER.addHandler(FILE_HANDLER)

PASSDEPLOY_LOGGER = logging.getLogger("passdeploy")
PASSDEPLOY_LOGGER.setLevel(logging.WARNING)

################################################################################

# create log file if it doesn't exist by creating a log entry
LOGGER.info("Begin testing web service plugins.")

# put in here the names (directory name) of the web service plugins
SERVICES = ('Google', 'Yahoo', 'Facebook', 'Wikipedia', 'Twitter')

PASSDEPLOY = passdeploy.Passdeploy(batch_mode=True)

# tracks if there were any problems with passphrase changes
error_occurred = False

for service in SERVICES:
    with open("credentials/" + service + '.txt', 'r+') as \
            current_credentials_file:
        # read credentials
        credentials = current_credentials_file.readline()
        try:
            user, current_passphrase = credentials.split(';', 2)
        except ValueError:
            LOGGER.error("Could not parse credentials of " + service)
            continue

        # change passphrase
        new_passphrase = generate_passphrase()

        ret = PASSDEPLOY.change_passphrase(service,
                                           user,
                                           current_passphrase,
                                           new_passphrase)

        if ret == 0:
            LOGGER.info("Passphrase change of " + service + " was successful.")

            # store old credentials
            with open("credentials/" + service + '.old-cred.txt', 'a') as \
                    old_credentials_file:
                old_credentials_file.write("\n" + credentials)

            # update current credentials to file by emptying it and writing
            # the new passphrase
            current_credentials_file.seek(0)
            current_credentials_file.truncate()
            current_credentials_file.write(user + ";" + new_passphrase)

        elif ret == 1:
            LOGGER.error("Passphrase change of " + service + " was NOT "
                                                             "successful.")
            error_occurred = True
        elif ret == 2:
            LOGGER.error(
                "Uncertainty on passphrase change for " + service + ".")
            error_occurred = True
        else:
            LOGGER.error("Undefined error condition with " + service +
                         " plugin.")
            error_occurred = True

if error_occurred:
    sys.exit(1)
else:
    sys.exit(0)
