#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""passdeploy is framework for automatizing passphrase changes and deployments.

It provides a basis to easily write service and deployment plugins to change
and deploy passphrase.

Currently it already contains service plugins from web service like Google,
Facebook, Wikipedia, but also a service plugin for handling passphrases of
SSH private key files.

On the deployment side, there are e.g. plugins for Firefox and
Chrome/Chromium available.

For more information, take a look at the README file and the docs/ directory.
"""

import logging
import os
import threading

from six import string_types

# set DEBUG logging level if you want to debug plugins
# logging.basicConfig(level=logging.DEBUG)

logger = logging.getLogger("passdeploy")


class Passdeploy:
    """Provides the core functionality of passdeploy."""

    def __init__(self, batch_mode=False):
        # load plugins
        self._service_plugins, self._deployment_plugins = self._load_plugins()

        if not batch_mode:
            from passdeploy.gui import Gui
            passdeploy_gui = Gui(self)
            passdeploy_gui.start()

    def change_passphrase(self, service, identity, passphrase, new_passphrase):
        """Changes a passphrase to new_passphrase with given authentication
        and service information.

        This function executes the change_passphrase function of a service
        plugin.

        :param service: Name of the service plugin (directory name).
        :type service: str
        :param identity: Identity information. E. g. username.
        :type identity: str
        :param passphrase: Current passphrase.
        :type passphrase: str
        :param new_passphrase: New passphrase.
        :type new_passphrase: str
        :return: int -- 0: if passphrase was changed successfully,
                        1: if passphrase couldn't be changed
                        2: if it's not sure whether passphrase was changed or
                           not
                        > 2: unexpected error conditions
                        3: if, service plugin is not supported/loaded
                        5: change_passphrase function of plugin return
                           unknown value
                        6: if, exception occurred while executing the service
                           plugin
        """

        if service not in self._service_plugins.keys():
            logger.error("Service plugin \"" + service + "\" is not supported."
                         " Maybe it wasn't loaded.")
            return 3

        plugin = self._service_plugins[service]

        logger.info("Changing passphrase.")

        try:
            ret = plugin['change_passphrase'](
                identity,
                passphrase,
                new_passphrase
            )

            if ret == 0:
                logger.info("Passphrase changed.")
                return ret
            elif ret == 1:
                logger.info("Passphrase wasn't changed.")
                return ret
            elif ret == 2:
                logger.info("It's not sure if passphrase was changed.")
                return ret
            else:
                logger.error("change_passphrase() of " + service + "service "
                              "plugin returned unknown value: " + ret)
                return 5
        except BaseException:
            logger.exception("Exception while executing "
                              + service + " service plugin.")
            return 6

    def deploy_passphrase(self, service, identity, old_passphrase,
                          new_passphrase, deployment_target):
        """Deploys new_passphrase with given authentication, service and
        deployment_target information.

        This function executes the deploy function of a deployment
        plugin.

        :param service: Name of the service plugin (directory name).
        :type service: str
        :param identity: Identity information. E. g. username.
        :type identity: str
        :param old_passphrase: Old passphrase which is currently deployed.
        :type old_passphrase: str
        :param new_passphrase: New passphrase which should be deployed.
        :type new_passphrase: str
        :param deployment_target: Name of the deployment plugin (directory
                                  name).
        :type deployment_target: str
        :return: int -- 0: if passphrase was deployed successfully,
                        1: if passphrase couldn't be deployed
                        2: if it's not sure whether passphrase was deployed
                           or not
                        > 2: unexpected error conditions
                        3: if, service plugin is not supported/loaded
                        4: if, deployment plugin is not supported/loaded
                        5: change_passphrase function of plugin return
                           unknown value
                        6: if, exception occurred while executing the deployment
                           plugin
        """

        if service not in self._service_plugins.keys():
            logger.error("Service plugin \"" + service + "\" is not supported."
                          " Maybe it wasn't loaded.")
            return 3

        if deployment_target not in self._deployment_plugins.keys():
            logger.error("Deployment plugin \"" + deployment_target + "\" is "
                          "not supported. Maybe it wasn't loaded.")
            return 4

        s_plugin = self._service_plugins[service]
        d_plugin = self._deployment_plugins[deployment_target]

        lock = d_plugin['lock']

        logger.debug("Waiting for lock.")

        with lock:
            logger.info("Deploying passphrase.")

            try:
                ret = d_plugin['exec'](
                    s_plugin['origin_urls'],
                    identity,
                    old_passphrase,
                    new_passphrase
                )
            except BaseException:
                logger.exception("Exception while executing deployment "
                                  "plugin.")
                return 6

        if ret == 0:
            logger.info("Passphrase was deployed.")
            return ret
        elif ret == 1:
            logger.info("Passphrase was not deployed.")
            return ret
        elif ret == 2:
            logger.info("It's not sure if passphrase was deployed.")
        else:
            logger.error("deploy() of " + deployment_target + "deployment "
                          "plugin returned unknown value: " + ret)
            return 5

    def get_services(self):
        """Returns list of supported services.

        :return: List[str]
        """
        return sorted(list(self._service_plugins.keys()), key=str.lower)

    def get_deployment_targets(self):
        """Returns list of supported deployment targets.

        :return: List[str]
        """
        return sorted(list(self._deployment_plugins.keys()), key=str.lower)

    def get_service_plugins(self):
        """Returns a dictionary of service plugins.

        See the _load_plugins docstring for more information.

        :return: dict -- dictionary of service plugins
        """
        return self._service_plugins

    def get_deployment_plugins(self):
        """Returns a dictionary of deployment plugins.

        See the _load_plugins docstring for more information.

        :return: dict -- dictionary of deployment plugins
        """
        return self._deployment_plugins

    @staticmethod
    def _load_plugins():
        """
        Load service and deployment plugins.
        :returns: tuple -- (service_plugins, deployment_plugins),
        where service_plugins and deployment_plugins are dictionaries.

        Structure of the deployment_plugins dict:
        {
            plugin_directory: {
                'exec': function,
                'lock': _thread.RLock
            }
        }

        plugin_directory is named by directory of the deployment plugin and is
        the key to a dict for more information provided by the plugin:
            * 'exec': key to the deploy function implemented by the deployment
                plugin.
            * 'lock': key to an RLock instance returned by threading.RLock()
                to prevent concurrent access to deployment targets.

        Structure of the service_plugins dict:
        {
            plugin_directory: {
                'origin_urls': List[str],
                'change_passphrase': function,
                'default_identity': List[str]
            }
        }

        plugin_directory is named by directory of the service plugin and is
        the key to a dict for more information provided by the  plugin:
            * 'origin_urls': key to the list of URLs related to the service.
            * 'change_passphrase': key to the change_passphrase function
                implemented by the service plugin.
            * 'default_identity': key to the list of default identities

        Example:
        {
            'Google': {
                'origin_urls': ['https://accounts.google.com/ServiceLogin'],
                'change_passphrase': imported_service_plugin.change_passphrase,
                'default_identity': ['admin','user','default_user']
            }
        }
        """

        SERVICE_PLUGINS_DIRECTORY = os.path.join( # pylint: disable=C0103
            os.path.dirname(__file__),
            "plugins/services/"
        )

        DEPLOYMENT_PLUGINS_DIRECTORY = os.path.join( # pylint: disable=C0103
            os.path.dirname(__file__),
            "plugins/deployment/"
        )

        def is_list_of_strings(lst):
            """Checks if lst is of type List[str].

            :param lst: List of str to be checked.
            :type lst: List[str]
            :return: bool -- True if lst is of type List[str], False otherwise.
            """
            return bool(lst) and isinstance(lst, list) and all(
                isinstance(elem, string_types) for elem in lst)

        plugins_directories = next(os.walk(SERVICE_PLUGINS_DIRECTORY))[1]

        service_plugins = {}

        for directory in plugins_directories:
            # Try to initialize the plugin by its initialization
            # function if it has one.
            # If it has one check the return value, if the return value
            # is a dict with default values (e. g. default values for
            # identities) store them in the service_plugins dict.

            if directory == '__pycache__':
                continue

            try:
                # TODO: look for a cleaner version, importlib didn't work
                # when using the setuptools installed executable for passdeploy
                import imp
                imported_module = imp.load_source(
                    directory,
                    os.path.join(SERVICE_PLUGINS_DIRECTORY, directory +
                                 '/main.py'))

                # module = importlib.import_module(
                #     'plugins.services.' + directory + '.main'
                # )

            except ImportError:
                logger.exception("Couldn't import " + directory +
                                  " service plugin.")
                continue

            try:
                if not hasattr(imported_module.change_passphrase, '__call__'):
                    logger.exception(directory + " plugin not loaded. There "
                                                  "is no change_passphrase "
                                                  "function.")
                    continue
            except AttributeError:
                logger.exception(directory + ' plugin not loaded. There is no '
                                              'change_passphrase function.')
                continue

            plugin = {
                'origin_urls': [],
                'change_passphrase': imported_module.change_passphrase,
                'default_identity': ['']
            }

            try:
                plugin['origin_urls'] = imported_module.origin_urls
            except AttributeError:
                logger.debug(directory + " plugin: No origin_urls.")
                # since origin_urls are optional do nothing

            if hasattr(imported_module, 'init_as_plugin'):
                try:
                    #
                    ret = imported_module.init_as_plugin()
                except BaseException:
                    logger.exception(directory + " plugin not loaded. "
                                                  "Exception in initialization "
                                                  "function.")
                    continue

                if ret == 0:
                    logger.debug(directory + " plugin successfully loaded. "
                                              "and no default values provided.")
                elif ret == 1:
                    logger.error(directory + " plugin not loaded. Plugin "
                                              "couldn't be initialized.")
                    continue

                # assume that default values were provided
                try:
                    if is_list_of_strings(ret['default_identity']):
                        plugin['default_identity'] = ret['default_identity']
                    else:
                        logger.error(directory + " plugin not loaded. "
                                                  "default_identity wasn't "
                                                  "populated properly.")
                        continue
                except (KeyError, TypeError):
                    # default_identity is optional
                    pass

            service_plugins.update({
                directory: plugin
            })

        # load deployment plugins
        plugins_directories = next(os.walk(DEPLOYMENT_PLUGINS_DIRECTORY))[1]

        deployment_plugins = {}

        for directory in plugins_directories:
            if directory == '__pycache__':
                continue

            try:
                # TODO: look for a cleaner version, importlib didn't work
                # when using the setuptools installed executable for passdeploy
                import imp
                imported_module = imp.load_source(
                    directory,
                    os.path.join(DEPLOYMENT_PLUGINS_DIRECTORY, directory +
                                 '/main.py'))
                # module = importlib.import_module(
                #    'plugins.deployment.' + directory + '.main'
                # )
            except ImportError:
                logger.exception("Couldn't import " + directory +
                                  " deployment plugin.")
                continue

            deployment_plugins.update({
                directory: {
                    'exec': imported_module.deploy,
                    'lock': threading.RLock()
                }
            })

        return service_plugins, deployment_plugins


def main():
    """Start passdeploy."""

    Passdeploy()


if __name__ == "__main__":
    main()
