# -*- coding: utf-8 -*-

import logging
from bs4 import BeautifulSoup


try:  # for Python 2 compatibility
    # noinspection PyCompatibility
    from urllib.parse import urljoin
except ImportError:
    # noinspection PyCompatibility,PyUnresolvedReferences
    from urlparse import urljoin

logger = logging.getLogger(__name__)


class ScraplibException(Exception):
    pass


def _get_parsed_html(raw_html):
    logger.debug("Parsing HTML.")
    return BeautifulSoup(raw_html, 'lxml')


def _get_input_data(parsed_html, form_number=1):
    logger.debug("Getting input data.")
    input_data = {}

    forms = parsed_html.find_all('form')

    if not forms:
        raise ScraplibException("No form was found.")

    # TODO: form_number not validated
    form = forms[form_number - 1]
    input_elements_of_form = form.find_all('input', {'name': True})

    for input_element in input_elements_of_form:
        input_data.update(
            {
                input_element['name']: input_element.get('value', '')
            }
        )

    # check for a button of type submit
    # scraplib can be fooled, if there are both input and button of type submit
    # or multiple buttons of type submit

    buttons = form.find_all('button', {'type': 'submit', 'name': True})
    if buttons:
        button = buttons[0]
        input_data.update({button['name']: button.get('value', '')})

    # TODO: check for other POST data sources such as <select>

    return input_data


def _get_form_url(parsed_html, current_url, form_number=1):
    logger.debug("Getting form url.")
    base_url = current_url
    form = parsed_html.find_all("form")

    if not form:
        raise ScraplibException("No form element found.")

    # TODO: no validation, possible KeyError
    # TODO: no validation of form_number
    if 'action' in form[form_number - 1].attrs:
        action_attr = form[form_number - 1]['action']
    else:  # in case there is no action attribute
        action_attr = ''

    # if there is a base-element, respect it
    # TODO: this could be non-standard conform
    tmp_base_url = parsed_html.find_all("base")
    if not tmp_base_url:
        return urljoin(base_url, action_attr)
    else:
        # TODO: no validation, possible KeyError
        return urljoin(tmp_base_url[0]["href"], action_attr)


def form_request_from_response(requests_obj, response, form_data,
                               form_number=1):
    parsed_html = _get_parsed_html(response.text)
    input_data = _get_input_data(parsed_html, form_number=form_number)
    input_data.update(form_data)
    logger.debug("POST data: " + str(input_data))
    form_url = _get_form_url(parsed_html, response.url, form_number)
    logger.debug("Form URL: " + form_url)
    return requests_obj.post(form_url, input_data)
