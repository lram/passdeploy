#!/usr/bin/env python
# coding=utf-8
import logging

import requests

from passdeploy.scraplib import form_request_from_response

logger = logging.getLogger(__name__)
# logging.basicConfig(level=logging.DEBUG)

origin_urls = ['https://m.facebook.com/',
               'https://www.facebook.com/']


def change_passphrase(identity, cur_pass, new_pass):
    """Takes the identity (username), the current passphrase and a new
    passphrase to change the current passphrase to the new passphrase.

    :param identity: Identity (username).
    :type identity: str
    :param cur_pass: Current passphrase.
    :type cur_pass: str
    :param new_pass: New passphrase.
    :type new_pass: str
    :return: int -- 0 on success; 1 on failure; 2, if it's not sure whether
             passphrase was changed or not.
    """
    try:
        session = requests.Session()

        # noinspection PyPep8
        response = session.get(
            'https://m.facebook.com/settings/account/?password&refid=70&ref=m_nux_wizard')

        if 'name="pass" type="password"' not in response.text:
            logger.warning('Could not detect login site.')
            return 1

        response = form_request_from_response(session, response, {
            'email': identity,
            'pass': cur_pass
        })

        if 'New password</label>' not in response.text:
            logger.warning('Could not visit passphrase changing site.')
            return 1
    except requests.RequestException as exc:
        # RequestsException occurred, but we know for sure, that the passphrase
        # wasn't changed
        logger.error("RequestsException occurred while doing uncritical "
                     "things: " + str(exc))
        logger.debug(exc, exc_info=True)  # print stack trace at DEBUG level
        return 1
    else:
        try:
            response = form_request_from_response(
                session,
                response,
                {
                    'old_password': cur_pass,
                    'new_password': new_pass,
                    'confirm_password': new_pass
                },
                form_number=2
            )

            if 'Password Changed</div>' not in response.text:
                logger.warning("Probably the password wasn't changed.")
                return 2
        except requests.RequestException as exc:
            logger.error("RequestsException occurred while doing critical "
                         "things: " + str(exc))
            logger.debug(exc, exc_info=True)  # print stack trace at DEBUG level
            return 2
        else:
            # successful passphrase change
            return 0


if __name__ == "__main__":
    change_passphrase(
        'testacc@mail.com',
        'test1',
        'test2'
    )
