#!/usr/bin/env python3
# coding=utf-8

import logging

import requests

from passdeploy.scraplib import form_request_from_response

logger = logging.getLogger(__name__)
# logging.basicConfig(level=logging.DEBUG)

SUBDOMAINS = (
    'ab', 'ace', 'af', 'ak', 'als', 'am', 'an', 'ang', 'ar', 'arc', 'arz', 'as',
    'ast', 'av', 'ay', 'az', 'azb', 'ba', 'bar', 'bat-smg', 'bcl', 'be',
    'be-tarask', 'bg', 'bh', 'bi', 'bjn', 'bm', 'bn', 'bo', 'bpy', 'br', 'bs',
    'bug', 'bxr', 'ca', 'cbk-zam', 'cdo', 'ce', 'ceb', 'ch', 'chr', 'chy',
    'ckb', 'co', 'cr', 'crh', 'cs', 'csb', 'cu', 'cv', 'cy', 'da', 'de', 'diq',
    'dsb', 'dv', 'dz', 'ee', 'el', 'eml', 'en', 'eo', 'es', 'et', 'eu', 'ext',
    'fa', 'ff', 'fi', 'fiu-vro', 'fj', 'fo', 'fr', 'frp', 'frr', 'fur', 'fy',
    'ga', 'gag', 'gan', 'gd', 'gl', 'glk', 'gn', 'gom', 'got', 'gu', 'gv', 'ha',
    'hak', 'haw', 'he', 'hi', 'hif', 'hr', 'hsb', 'ht', 'hu', 'hy', 'ia', 'id',
    'ie', 'ig', 'ik', 'ilo', 'io', 'is', 'it', 'iu', 'ja', 'jbo', 'jv', 'ka',
    'kaa', 'kab', 'kbd', 'kg', 'ki', 'kk', 'kl', 'km', 'kn', 'ko', 'koi', 'krc',
    'ks', 'ksh', 'ku', 'kv', 'kw', 'ky', 'la', 'lad', 'lb', 'lbe', 'lez', 'lg',
    'li', 'lij', 'lmo', 'ln', 'lo', 'lrc', 'lt', 'ltg', 'lv', 'mai', 'map-bms',
    'mdf', 'mg', 'mhr', 'mi', 'min', 'mk', 'ml', 'mn', 'mo', 'mr', 'mrj', 'ms',
    'mt', 'mwl', 'my', 'myv', 'mzn', 'na', 'nah', 'nap', 'nds', 'nds-nl', 'ne',
    'new', 'nl', 'nn', 'no', 'nov', 'nrm', 'nso', 'nv', 'ny', 'oc', 'om', 'or',
    'os', 'pa', 'pag', 'pam', 'pap', 'pcd', 'pdc', 'pfl', 'pi', 'pih', 'pl',
    'pms', 'pnb', 'pnt', 'ps', 'pt', 'qu', 'rm', 'rmy', 'rn', 'ro', 'roa-rup',
    'roa-tara', 'ru', 'rue', 'rw', 'sa', 'sah', 'sc', 'scn', 'sco', 'sd', 'se',
    'sg', 'sh', 'si', 'simple', 'sk', 'sl', 'sm', 'sn', 'so', 'sq', 'sr', 'srn',
    'ss', 'st', 'stq', 'su', 'sv', 'sw', 'szl', 'ta', 'te', 'tet', 'tg', 'th',
    'ti', 'tk', 'tl', 'tn', 'to', 'tpi', 'tr', 'ts', 'tt', 'tum', 'tw', 'ty',
    'tyv', 'udm', 'ug', 'uk', 'ur', 'uz', 've', 'vec', 'vep', 'vi', 'vls', 'vo',
    'wa', 'war', 'wo', 'wuu', 'www', 'xal', 'xh', 'xmf', 'yi', 'yo', 'za',
    'zea', 'zh', 'zh-classical', 'zh-min-nan', 'zh-yue', 'zu'
)

origin_urls = []

for SUBDOMAIN in SUBDOMAINS:
    origin_urls.extend(
        [
            'https://' + SUBDOMAIN + '.wikipedia.org/w/index.php',
            'https://' + SUBDOMAIN + '.wikipedia.org/wiki/Special:UserLogin',
            'https://' + SUBDOMAIN + '.wikipedia.org'
        ]
    )


def change_passphrase(identity, cur_pass, new_pass):
    """Takes the identity (username), the current passphrase and a new
    passphrase to change the current passphrase to the new passphrase.

    :param identity: Identity (username).
    :type identity: str
    :param cur_pass: Current passphrase.
    :type cur_pass: str
    :param new_pass: New passphrase.
    :type new_pass: str
    :return: int -- 0 on success; 1 on failure; 2, if it's not sure whether
             passphrase was changed or not.
    """

    try:
        session = requests.Session()

        # assumption: passphrase change for non-english Wikipedia
        # credentials works on english Wikipedia

        response = session.get(
            'https://en.wikipedia.org/w/index.php?title=Special:ChangePassword')

        if 'name="wpPassword"' not in response.text:
            logger.warning('Could not detect login site.')
            return 1

        response = form_request_from_response(session, response, {
            'wpName': identity,
            'wpPassword': cur_pass
        })

        if 'Change credentials</h1>' not in response.text:
            logger.warning('Could not visit passphrase changing site.')
            return 1
    except requests.RequestException as exc:
        # RequestsException occurred, but we know for sure, that the
        # passphrase wasn't changed
        logger.error("RequestsException occurred while doing uncritical "
                     "things: " + str(exc))
        logger.debug(exc, exc_info=True)  # print stack trace at DEBUG level
        return 1
    else:
        try:
            response = form_request_from_response(
                session,
                response,
                {
                    'password': new_pass,
                    'retype': new_pass
                }
            )

            if 'Your credentials have been changed.' not in response.text:
                logger.warning("Probably the password wasn't changed.")
                return 2
        except requests.RequestException as exc:
            logger.error("RequestsException occurred while doing critical "
                         "things: " + str(exc))
            logger.debug(exc,
                         exc_info=True)  # print stack trace at DEBUG level
            return 2
        else:
            # successful passphrase change
            return 0


if __name__ == "__main__":
    change_passphrase(
        'wiki-username',
        'pass',
        'newpass'
    )
