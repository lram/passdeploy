#!/usr/bin/env python3
# coding=utf-8

import logging

import requests

from passdeploy.scraplib import form_request_from_response

logger = logging.getLogger(__name__)
# logging.basicConfig(level=logging.DEBUG)

origin_urls = [
    'https://mobile.twitter.com/session/new',
    'https://mobile.twitter.com/',
    'https://twitter.com/',
    'https://mobile.twitter.com/login'
]


def change_passphrase(identity, cur_pass, new_pass):
    """Takes the identity (username), the current passphrase and a new
    passphrase to change the current passphrase to the new passphrase.

    :param identity: Identity (username).
    :type identity: str
    :param cur_pass: Current passphrase.
    :type cur_pass: str
    :param new_pass: New passphrase.
    :type new_pass: str
    :return: int -- 0 on success; 1 on failure; 2, if it's not sure whether
             passphrase was changed or not.
    """

    try:
        session = requests.Session()

        response = session.get('https://mobile.twitter.com/session/new')

        if '<title>Log in</title>' not in response.text:
            logger.warning("Couldn't detect login page.")
            return 1

        form_request_from_response(session, response, {
            'session[username_or_email]': identity,
            'session[password]': cur_pass
        })

        response = session.get('https://mobile.twitter.com/settings/password')

        if 'settings[password_confirmation]' not in response.text:
            logger.warning("Couldn't detect passphrase changing page. "
                           "(Probably the login was not successful.)")
            return 1
    except requests.RequestException as exc:
        # RequestsException occurred, but we know for sure, that the passphrase
        # wasn't changed
        logger.error("RequestsException occurred while doing uncritical "
                     "things: " + str(exc))
        logger.debug(exc, exc_info=True)  # print stack trace at DEBUG level
        return 1
    else:
        try:
            response = form_request_from_response(session, response, {
                'settings[current_password]': cur_pass,
                'settings[password]': new_pass,
                'settings[password_confirmation]': new_pass
            })

            if response.url == 'https://mobile.twitter.com/settings/password':
                logger.warning('Still on passphrase changing site. Passphrase '
                               'not changed')
                return 1
            elif response.url == 'https://mobile.twitter.com/settings':
                # assume redirect to /setting does mean successful passphrase
                # change
                return 0
            else:
                logger.warning("Probably the password wasn't changed.")
                return 2
        except requests.RequestException as exc:
            logger.error("RequestsException occurred while doing critical "
                         "things: " + str(exc))
            logger.debug(exc, exc_info=True)  # print stack trace at DEBUG level
            return 2


if __name__ == "__main__":
    change_passphrase(
        'username',
        'current-passphrase',
        'new-passphrase'
    )
