#!/usr/bin/env python3
# coding=utf-8
from subprocess import Popen, PIPE
import os
import logging

# TODO
# WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING
# Passphrase isn't given by stdin but by command line args, so it is
# leaked to the process list. So this plugin shouldn't be used on multi-user
# systems.
# WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING

logger = logging.getLogger(__name__)
# logging.basicConfig(level=logging.DEBUG)


def init_as_plugin():
    """
    Get all private key file names (id_* without file extension)
    and give them to the GUI to present default values.
    """

    data = {"default_identity": []}

    # assume that POSIX system is storing SSH keys in ~/.ssh/
    if os.name == "posix":
        ssh_directory = os.getenv('HOME') + '/.ssh/'
        for filename in os.listdir(ssh_directory):
            # we don't want public keys
            if filename.startswith("id_") and '.' not in filename:
                data["default_identity"].append(ssh_directory + filename)

    if data['default_identity']:
        return data
    else:
        return 0


def change_passphrase(identity, cur_pass, new_pass):
    """Takes the identity (absolute path to private key file), the current
    passphrase and a new passphrase to change the current passphrase to the
    new passphrase.

    :param identity: Identity (absolute path to private key file).
    :type identity: str
    :param cur_pass: Current passphrase.
    :type cur_pass: str
    :param new_pass: New passphrase.
    :type new_pass: str
    :return: int -- 0 on success; 1 on failure; 2, if it's not sure whether
             passphrase was changed or not.
    """

    if not identity:
        identity = os.path.join(os.path.expanduser('~') + '/.ssh/id_rsa')
        logger.debug(identity)

    args = ('/usr/bin/ssh-keygen', '-p', '-f', "/home/user/.ssh/id_ed25519",
            "-P", cur_pass, "-N", new_pass)

    popen = Popen(args, stdin=PIPE, stdout=PIPE, stderr=PIPE)

    output = popen.communicate()[0]

    if popen.returncode == 0:
        # success
        return 0
    else:
        if "Bad passphrase." in output.decode():
            logger.warning("Passphrase is wrong.")
            return 1
        else:
            logger.error("Undefined state.")
            return 2


if __name__ == "__main__":
    change_passphrase(
        '/home/user/.ssh/id_ecdsa',
        'asdfasdf1',
        'yxcvyxcv'
    )
