#!/usr/bin/env python
# coding=utf-8

import logging

import requests

from passdeploy.scraplib import form_request_from_response

logger = logging.getLogger(__name__)
# logging.basicConfig(level=logging.DEBUG)

origin_urls = ['https://accounts.google.com/ServiceLogin']


def change_passphrase(identity, cur_pass, new_pass):
    """Takes the identity (username), the current passphrase and a new
    passphrase to change the current passphrase to the new passphrase.

    :param identity: Identity (username).
    :type identity: str
    :param cur_pass: Current passphrase.
    :type cur_pass: str
    :param new_pass: New passphrase.
    :type new_pass: str
    :return: int -- 0 on success; 1 on failure; 2, if it's not sure whether
             passphrase was changed or not.
    """
    try:
        session = requests.Session()

        response = session.get(
            'https://myaccount.google.com/general-light/password'
        )

        response = form_request_from_response(
            session,
            response,
            {'Email': identity}
        )

        if '<input id="Passwd" name="Passwd"' not in response.text:
            logger.warning('Identity was rejected.')
            return 1

        # TODO: fix captcha detection functionality
        # uncommented because of false positives

        # if 'captcha-box' in response.text:
        #    logger.warning('Could not continue because of CAPTCHA.')
        #    return 1

        response = form_request_from_response(
            session,
            response,
            {'Passwd': cur_pass}
        )

        if 'New password' not in response.text:
            logger.warning("Couldn't visit passphrase changing site.")
            return 1
    except requests.RequestException as exc:
        logger.error(
            "RequestsException occurred while doing uncritical "
            "things.")
        logger.debug(exc, exc_info=True)  # print stack trace at DEBUG level
        return 1
    else:
        try:
            response = form_request_from_response(
                session,
                response,
                form_data={
                    'id.boq.ubcp.password': new_pass,
                    'id.boq.ubcp.repeatedPassword': new_pass
                }
            )

            if 'Account settings</h1>' not in response.text:
                logger.warning(
                    "Probably the password wasn't changed, since there was "
                    "no redirect to the account settings site.")
                return 2
        except requests.RequestException as exc:
            logger.error("RequestsException occurred while doing critical "
                         "things: " + str(exc))
            logger.debug(exc, exc_info=True)  # print stack trace at DEBUG level
        else:
            return 0


if __name__ == "__main__":
    change_passphrase(
        'test@gmail.com',
        'cur_pass',
        'new_pass'
    )
