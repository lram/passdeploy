# coding=utf-8

import logging

import requests

from passdeploy.scraplib import form_request_from_response

logger = logging.getLogger(__name__)
# logging.basicConfig(level=logging.DEBUG)

origin_urls = ['https://login.yahoo.com/',
               'https://login.yahoo.com/config/login',
               'https://login.yahoo.com/account/create'
              ]


def change_passphrase(identity, cur_pass, new_pass):
    """Takes the identity (username), the current passphrase and a new
    passphrase to change the current passphrase to the new passphrase.

    :param identity: Identity (username).
    :type identity: str
    :param cur_pass: Current passphrase.
    :type cur_pass: str
    :param new_pass: New passphrase.
    :type new_pass: str
    :return: int -- 0 on success; 1 on failure; 2, if it's not sure whether
             passphrase was changed or not.
    """
    try:
        session = requests.Session()

        response = session.get(
            'https://login.yahoo.com/account/change-password'
        )

        if 'name="username"' not in response.text:
            logger.warning('Could not detect login site.')
            return 1

        response = form_request_from_response(
            session,
            response,
            {'username': identity}
        )

        if 'name="passwd"' not in response.text:
            logger.warning('Could not detect passphrase field on login site.')
            return 1

        response = form_request_from_response(
            session,
            response,
            {'passwd': cur_pass}
        )

        if 'name="confirmPassword"' not in response.text:
            logger.warning('Could not detect passphrase changing site.')
            return 1
    except requests.RequestException as exc:
        # RequestsException occurred, but we know for sure, that the passphrase
        # wasn't changed
        logger.error("RequestsException occurred while doing uncritical "
                     "things: " + str(exc))
        logger.debug(exc, exc_info=True)  # print stack trace at DEBUG level
        return 1
    else:
        try:
            response = form_request_from_response(
                session,
                response,
                {
                    'password': new_pass,
                    'confirmPassword': new_pass
                }
            )

            if '<h1 class="heading cpwSuccess">' not in response.text:
                logger.warning("Probably the passphrase wasn't changed.")
                return 2
        except requests.RequestException as exc:
            logger.error("RequestsException occurred while doing critical "
                         "things: " + str(exc))
            logger.debug(exc, exc_info=True)  # print stack trace at DEBUG level
            return 2
        else:
            # successful passphrase change
            return 0


if __name__ == "__main__":
    change_passphrase(
        'testacc',
        'current passphrase',
        'new passphrase'
    )
