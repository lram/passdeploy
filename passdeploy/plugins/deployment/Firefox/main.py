#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Based on original work from: www.dumpzilla.org
# Based on intermediate work from:
# https://github.com/Unode/firefox_decrypt/blob/master/firefox_decrypt.py

import json
import logging
import os
from base64 import b64decode, b64encode
from ctypes import c_uint, c_void_p, c_char_p, cast, byref, string_at, \
    Structure, CDLL

try:
    # noinspection PyCompatibility
    from configparser import ConfigParser
except ImportError:
    # noinspection PyCompatibility,PyUnresolvedReferences
    from ConfigParser import ConfigParser
import shutil
import time

logger = logging.getLogger(__name__)


# logging.basicConfig(level=logging.DEBUG)

class _FirefoxProfile:
    """
    Loads Firefox profiles and can give profiles to users.
    """

    def __init__(self):
        self._profiles_path = self._get_profiles_path()
        _parsed_profile_ini = self._read_profile_ini(self._profiles_path)
        self._profile_names = self._get_profile_names(
            _parsed_profile_ini
        )

    class Exception(Exception):
        pass

    @staticmethod
    def _get_profiles_path():
        """
        Returns the absolute path (depended on the operating system) to the
        directory which contains the profiles directory.

        :return: str -- absolute path to the directory which contains the 
                       profiles directory
        """

        if os.name == "nt":
            profile_path = os.path.join(os.environ['APPDATA'], "Mozilla",
                                        "Firefox")
        elif os.uname()[0] == "Darwin":
            profile_path = "~/Library/Application Support/Firefox"
        else:
            profile_path = "~/.mozilla/firefox"

        return os.path.expanduser(profile_path)

    @staticmethod
    def _get_profile_names(parsed_profile_ini):
        """
        Returns the profile names by reading the parsed profile.ini
        file.

        Example of a profile name: 7gnb0qs2.default

        :param parsed_profile_ini: parsed profile.ini file from 
                                   _read_profile_ini()
        :type parsed_profile_ini: ConfigParser instance
        :return: List[str] -- profile names
        """
        profile_names = []

        for section in parsed_profile_ini.sections():
            if section.startswith("Profile"):
                profile_names.append(parsed_profile_ini.get(section, "Path"))

        logger.debug("Profiles: " +  str(profile_names))

        return profile_names

    @staticmethod
    def _read_profile_ini(profiles_path):
        """
        Reads the profile.ini file with ConfigParser and returns the instance of
        the ConfigParser instance.

        :param profiles_path:
        :type profiles_path: str
        :return:
        """

        profile_ini_path = os.path.join(profiles_path, "profiles.ini")

        logger.debug("Reading profiles from %s", profile_ini_path)

        if not os.path.isfile(profile_ini_path):
            raise _FirefoxProfile.Exception(
                "_profile.ini not found in %s", profiles_path
            )

        # Read profiles from Firefox _profile folder
        parsed_profile_ini = ConfigParser()
        parsed_profile_ini.read(profile_ini_path)

        return parsed_profile_ini

    def get_profile_path(self, profile_name=None):
        if profile_name:
            # TODO: not implemented
            raise _FirefoxProfile.Exception("Not implemented.")
        else:
            # return default profile
            profile_path = os.path.join(
                self._profiles_path,
                self._profile_names[0]
            )

            if not os.path.isdir(profile_path):
                raise _FirefoxProfile.Exception(
                    "Profile location '%s' is not a directory. Has "
                    "profiles.ini been tampered with?",
                    profile_path
                )

            return profile_path


class _FirefoxJSONCredentials:
    """Used to open Firefox' logins.json file, which contains stored
    credentials.
    """

    def __init__(self, profile_path, master_passphrase=''):
        self._credentials_db_path = os.path.join(profile_path, "logins.json")
        self._profile = profile_path
        self._master_passphrase = master_passphrase

        # backup credentials database
        date = time.strftime("%Y%m%d", time.gmtime())
        timestamp = str(int(time.time()))
        backup_filename = \
            date + "-" + timestamp + '-passdeploy-backup.logins.json'

        credentials_db_backup_path = os.path.join(profile_path, backup_filename)
        shutil.copy(self._credentials_db_path, credentials_db_backup_path)

    class Exception(Exception):
        pass

    def change_credentials(self, origin_urls, identity, old_pass, new_pass):
        """
        :param origin_urls: list of URLs related to identity and passphrase
        :type origin_urls: list of str
        :param identity: identity, e. g. username/email
        :type identity: str
        :param old_pass: passphrase, which is currently stored
        :type old_pass: str
        :param new_pass: new passphrase, which will replace the currently stored
        :type new_pass: str
        :return: True if data was changed, False if not and raise exception on
                 failure
        """

        # load logins.js as JSON object
        json_data = None
        data_changed = False

        with open(self._credentials_db_path) as credentials_db:
            try:
                json_data = json.load(credentials_db)
                logins = json_data["logins"]
            except:
                raise _FirefoxJSONCredentials.Exception(
                    "Couldn't read logins from logins.json."
                )
            else:
                # create NSSInteraction object to decrypt passphrases
                nss = _NSS(self._profile, self._master_passphrase)

                for login in logins:
                    if login["hostname"] in origin_urls:
                        logger.debug("Hostname matched.")
                        # enctype informs if passwords are encrypted and
                        # protected by a master password
                        if login["encType"]:
                            user, passw = nss.decrypt_entry(
                                login["encryptedUsername"],
                                login["encryptedPassword"]
                            )
                        else:
                            # we don't handle this case, since we assume
                            # encType should always be 1
                            logger.warning("Not implemented. Hostname found, "
                                           "but credentials are unencrypted. "
                                           "(enctype = 0 in logins.js)")
                            continue

                        if identity == user and old_pass == passw:
                            logger.debug(
                                "Username and passphrase are matching."
                            )
                            enc_u, enc_p = nss.encrypt_entry(user, new_pass)
                            login['encryptedUsername'] = enc_u
                            login['encryptedPassword'] = enc_p

                            data_changed = True

                # close/shutdown nss instance
                nss.shutdown()

        if not data_changed:
            logger.debug("No data was changed.")
            return False

        # TODO: race condition
        with open(self._credentials_db_path, "w") as credentials_db:
            try:
                credentials_db.write(json.dumps(json_data))
                return True
            except:
                raise _FirefoxJSONCredentials.Exception(
                    "Couldn't access JSON data while writing to the new "
                    "logins.js file."
                )


class _NSS(object):
    """
    Interact with libnss
    """

    def __init__(self, profile, password):
        self.NSS = self._load_libnss()
        self._initialize_libnss(profile, password)

    class _SECItem(Structure):
        """struct needed to interact with libnss
        """

        _fields_ = [('type', c_uint), ('data', c_void_p), ('len', c_uint)]

        def __init__(self, _type=None, _data=None, _len=None):
            if _type:
                self.type = _type
            if _data:
                self.data = _data
            if _len:
                self.len = _len

            print()

    class Exception(Exception):
        pass

    def _load_libnss(self):
        """Load libnss into python using the CDLL interface
        """
        if os.name == "nt":
            libnss_filename = "nss3.dll"
            locations = (
                "",  # Current directory or system lib finder
                r"C:\Program Files (x86)\Mozilla Firefox",
                r"C:\Program Files\Mozilla Firefox"
            )
            firefox = self._find_nss(locations, libnss_filename)

            os.environ["PATH"] = ';'.join([os.environ["PATH"], firefox])
            logger.debug("PATH is now %s", os.environ["PATH"])

        elif os.uname()[0] == "Darwin":
            libnss_filename = "libnss3.dylib"
            locations = (
                "",  # Current directory or system lib finder
                "/usr/local/lib/nss",
                "/usr/local/lib",
                "/opt/local/lib/nss",
                "/sw/lib/firefox",
                "/sw/lib/mozilla",
            )

            firefox = self._find_nss(locations, libnss_filename)
        else:
            libnss_filename = "libnss3.so"
            firefox = ""  # Current directory or system lib finder

        try:
            libnss_path = os.path.join(firefox, libnss_filename)
            logger.debug("Loading NSS library from %s", libnss_path)

            libnss = CDLL(libnss_path)

        except Exception as ex:
            logger.error(
                "Problems opening '%s' required for password decryption",
                libnss_filename
            )
            logger.error("Error was %s", ex)
            raise _NSS.Exception("Failed to load libnss.")

        return libnss

    @staticmethod
    def _find_nss(locations, nssname):
        """Locate nss is one of the many possible locations
        """
        for loc in locations:
            if os.path.exists(os.path.join(loc, nssname)):
                return loc

        logger.warning("%s not found on any of the default locations for this "
                       "platform. Attempting to continue nonetheless.", nssname)
        return ""

    def _handle_error(self):
        """If an error happens in libnss, handle it and print some debug information
        """
        logger.debug(
            "Error during a call to NSS library, trying to obtain error info"
        )

        error = self.NSS.PORT_GetError()
        self.NSS.PR_ErrorToString.restype = c_char_p
        self.NSS.PR_ErrorToName.restype = c_char_p
        error_str = self.NSS.PR_ErrorToString(error)
        error_name = self.NSS.PR_ErrorToName(error)

        error_name = error_name.decode("utf8")
        error_str = error_str.decode("utf8")

        logger.debug("%s: %s", error_name, error_str)

    def _get_keyslot(self):
        """Obtain a pointer to the internal key slot"""

        # NOTE This code used to be:
        #   return self.NSS.PK11_GetInternalKeySlot()
        # but on some systems this would segfault.
        # I don't quite understand the reasons but forcing the output to be
        # treated as a pointer and passed again as a pointer avoids the segfault

        self.NSS.PK11_GetInternalKeySlot.restype = c_void_p
        internal_keyslot = self.NSS.PK11_GetInternalKeySlot()
        return cast(internal_keyslot, c_void_p)

    def _initialize_libnss(self, profile, password):
        """Initialize the NSS library by authenticating with the user supplied password
        """
        logger.debug("Initializing NSS with profile path '%s'", profile)

        i = self.NSS.NSS_Init(profile.encode("utf8"))
        logger.debug("Initializing NSS returned %s", i)

        if i != 0:
            logger.error("Couldn't initialize NSS, maybe '%s' is not a valid "
                         "_profile?", profile)
            self._handle_error()
            raise _NSS.Exception("Failed to initialize libnss.")

        if password:
            logger.debug("Retrieving internal key slot")
            p_password = c_char_p(password.encode("utf8"))
            keyslot = self._get_keyslot()

            logger.debug("Internal key slot %s", keyslot)

            if not keyslot:
                self._handle_error()
                raise _NSS.Exception("Failed to retrieve internal KeySlot.")

            logger.debug("Authenticating with password '%s'", password)

            i = self.NSS.PK11_CheckUserPassword(keyslot, p_password)
            logger.debug("Checking user password returned %s", i)

            if i != 0:
                self._handle_error()
                raise _NSS.Exception("Master password is not correct.")
        else:
            logger.debug("Attempting decryption without Master Password.")

    def _encrypt_data(self, data):
        data_reply = self._SECItem()
        data_request = self._SECItem(
            _data=cast(c_char_p(data.encode("utf-8")), c_void_p),
            _len=len(data.encode("utf-8"))
        )
        data_keyid = self._SECItem(0, 0, 0)

        i = self.NSS.PK11SDR_Encrypt(
            byref(data_keyid),
            byref(data_request),
            byref(data_reply),
            0
        )

        if i != 0:
            logger.debug("PK11SDR_Encrypt returned %s", i)
            self._handle_error()
            raise _NSS.Exception("Unexpected error after encryption.")

        return b64encode(
            string_at(data_reply.data, data_reply.len)
        ).decode("utf-8")

    def _decrypt_data(self, data):
        """

        :param data:
        :type data: str -- base64 encoded
        :return:
        """
        input_SECItem = self._SECItem(
            _data=cast(c_char_p(b64decode(data)), c_void_p),
            _len=len(b64decode(data))
        )

        output_SECItem = self._SECItem()

        logger.debug("Decrypting data: '%s'", data)

        i = self.NSS.PK11SDR_Decrypt(
            byref(input_SECItem),
            byref(output_SECItem),
            None
        )

        if i != 0:
            logger.debug("PK11SDR_Decrypt returned %s", i)
            self._handle_error()
            raise _NSS.Exception("Could not decrypt. Wrong master passphrase?")

        return (string_at(
            output_SECItem.data,
            output_SECItem.len
        )).decode('utf-8')

    def decrypt_entry(self, user, passw):
        """Decrypt one entry in the database
        """

        logger.debug("Decrypting user_encrypted")
        user_unencrypted = self._decrypt_data(user)

        logger.debug("Decrypting passw_encrypted")
        passw_unencrypted = self._decrypt_data(passw)

        return user_unencrypted, passw_unencrypted

    def encrypt_entry(self, user, passw):
        logger.debug("Encrypting user")
        user_encrypted = self._encrypt_data(user)

        logger.debug("Encrypting passphrase")
        password_encrypted = self._encrypt_data(passw)

        return user_encrypted, password_encrypted

    def shutdown(self):
        self.NSS.NSS_Shutdown()


def deploy(origin_urls, identity, old_pass, new_pass):
    try:
        firefox_profile = _FirefoxProfile()

        # open Firefox credentials with empty master passphrase and default
        # profile
        credentials = _FirefoxJSONCredentials(
            firefox_profile.get_profile_path()
        )
    except (_FirefoxProfile.Exception, _FirefoxJSONCredentials.Exception) as \
            exc:
        logger.error("Exception while preparing for passphrase change. "
                     + str(exc))
        logger.debug(exc, exc_info=True)  # print stack trace at DEBUG level
        return 1
    try:
        ret = credentials.change_credentials(
            origin_urls,
            identity,
            old_pass,
            new_pass
        )

        if ret:
            return 0
        else:
            return 1
    except (_FirefoxProfile.Exception, _FirefoxJSONCredentials.Exception) as \
                    exc:
        logger.error("Exception while preparing for passphrase change. "
                     + str(exc))
        logger.debug(exc, exc_info=True)  # print stack trace at DEBUG level
        logger.exception("Exception while changing credentials. Not sure if "
                         "whether deployment was successful or not.")
        return 2


if __name__ == "__main__":
    exit(
        deploy(
            ['https://en.wikipedia.org'],
            'User',
            'old-passphrase',
            'new-passphrase'
        )
    )
