#!/usr/bin/env python
# coding=utf-8
"""
This plugin deploys a passphrase into Chrome/Chromiums passphrase database (
Login data file)

# TODO: Currently only the default profile is regarded.

Code pieces from chromepass (licensed under GPL)
https://github.com/hassaanaliw/chromepass/blob/master/chromepass.py
"""

import os
import sys
import sqlite3

try:
    import win32crypt
except:
    pass
import logging


logger = logging.getLogger(__name__)
# logging.basicConfig(level=logging.DEBUG)


def _get_path():
    """Returns Chrome/Chromiums config path.
    :return: str -- str, if path found, None  on error.
    """
    if os.name == "nt":
        # This is the Windows Path
        path = os.getenv('localappdata') \
               + '\\Google\\Chrome\\User Data\\Default\\'
        if not os.path.isdir(path):
            logger.error("Couldn't find Chrome/Chromium config path.")
            return None
    elif (os.name == "posix") and (sys.platform == "darwin"):
        # This is the OS X Path
        path = os.getenv('HOME') \
               + "/Library/Application Support/Google/Chrome/Default/"
        if not os.path.isdir(path):
            logger.error("Couldn't find Chrome/Chromium config path.")
            return None
    elif os.name == "posix":
        # This is the Linux Path
        path = os.getenv('HOME') + '/.config/google-chrome/Default/'
        if not os.path.isdir(path):
            path = os.getenv('HOME') + '/.config/chromium/Default/'
            if not os.path.isdir(path):
                logger.error("Couldn't find Chrome/Chromium config path.")
                return None
    else:
        logger.error("Operating system not supported.")
        return None

    return path


def deploy(origin_urls, identity, old_pass, new_pass):
    """
    Checks if an entry exist, which matches the origin_urls,
    identity (username) and old_pass (current passphrase) and
    replaces old_pass with new_pass (new passphrase)
    """

    path = _get_path()

    if not path:
        return 1

    try:
        connection = sqlite3.connect(path + "Login Data")
        with connection:
            cursor = connection.cursor()
            affected = False
            for origin_url in origin_urls:
                result = cursor.execute(
                    "UPDATE logins SET password_value=? WHERE "
                    "origin_url=? AND "
                    "username_value=? AND "
                    "password_value=?",
                    (
                        # new_pass and old_pass have to be encoded because the
                        # type of password_value is BLOB
                        new_pass.encode(),
                        origin_url,
                        identity,
                        old_pass.encode()
                    )
                )

                if result.rowcount != 0:
                    affected = True

                connection.commit()
            if not affected:
                logger.warning("The database wasn't changed. Probably the "
                               "old passphrase isn't stored in the "
                               "database.'")
    except sqlite3.OperationalError as exc:
        exc_str = str(exc)
        if exc_str == 'database is locked':
            logger.error('Database is locked. Make sure Chrome/Chromium is not '
                         'running in the background')
            return 1
        elif exc_str == 'no such table: logins':
            logger.error('No information found in the database (no logins '
                         'table).')
            return 1
        elif exc_str == 'unable to open database file':
            logger.error('Could not open database file. Database path correct?')
            return 1
        else:
            logger.error(exc_str)
            return 2

    connection.close()

    if not affected:
        return 1
    else:
        return 0


if __name__ == '__main__':
    deploy(['https://accounts.google.com/ServiceLogin'],
           'test@gmail.com',
           'old-passphrase',
           'new-passphrase'
          )
