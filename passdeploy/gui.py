# coding=utf-8
"""Provides a GUI written in Tkinter for passdeploy.
"""

import logging
from threading import Thread

try:
    import tkinter as tk
except ImportError:
    # noinspection PyUnresolvedReferences,PyPep8Naming
    import Tkinter as tk
try:
    # noinspection PyCompatibility
    from tkinter.ttk import Combobox, Label, Frame, Button, Entry, Style, \
        Notebook
except ImportError:
    # noinspection PyCompatibility,PyUnresolvedReferences
    from ttk import Combobox, Label, Frame, Button, Entry, Style, Notebook

import passdeploy.change_history_table as cht


class Gui:
    """GUI class for the passdeploy framework."""

    def __init__(self, passdeploy):
        """Initializes GUI.

        :param passdeploy: Passdeploy object to access its functions.
        :type passdeploy: Passdeploy
        """

        self._passdeploy = passdeploy

        # globally access variable which contains the
        # configuration (service, identity, passphrase, new passphrase,
        # deployment target and status) of all entries within the GUI
        # accessed by _gui_change_passphrases() when button is pressed
        self._configurations = []

        self._logging_area = None

    def start(self):
        """Creates the GUI.

        :return: None
        """

        self._build_gui()

    def _build_gui(self):
        """Constructs the GUI with its rows, buttons and styles.

        :return: None
        """

        DEFAULT_WIDTH = 15  # pylint: disable=C0103

        # number of default entries
        NUM_DEFAULT_ENTRIES = 10  # pylint: disable=C0103

        root = tk.Tk()
        root.resizable(width=False, height=False)
        root.title('passdeploy')

        # build and configure window
        mainframe = Frame(root, padding='10 10 10 10')
        mainframe.grid(column=0, row=0, sticky=(tk.N, tk.W, tk.E, tk.S))

        titles = (
            'Service', 'Identity', 'Passphrase', 'New passphrase', 'Deploy on',
            'Status'
        )

        for i in range(0, len(titles)):
            label = Label(mainframe,
                          text=titles[i],
                          anchor='center',
                          width=15
                    )
            label.grid(column=i, row=0)

        for i in range(0, NUM_DEFAULT_ENTRIES):
            self._configurations.append(
                self._Configuration(
                    mainframe, 2 * i + 1,
                    self._passdeploy.get_service_plugins(),
                    self._passdeploy.get_deployment_plugins(),
                    width=DEFAULT_WIDTH
                )
            )

        btn_change_passphrases = Button(
            mainframe,
            width=DEFAULT_WIDTH,
            text="Change logins",
            command=self._gui_change_passphrases
        )
        btn_deploy = Button(
            mainframe,
            width=DEFAULT_WIDTH,
            text="Deploy",
            command=self._gui_deploy_changes
        )

        btn_change_passphrases.grid(column=4,
                                    row=2 * NUM_DEFAULT_ENTRIES + 1)
        btn_deploy.grid(column=4, row=2 * NUM_DEFAULT_ENTRIES + 2)

        self._logging_area = self._LoggingArea(
            master=mainframe,
            width=950,
            height=150
        )

        self._logging_area.grid(
            column=0,
            row=2 * NUM_DEFAULT_ENTRIES + 3,
            columnspan=6,
            rowspan=2,
            sticky=tk.W
        )

        logging.getLogger('').addHandler(
            self._logging_area.get_log_widget_handler()
        )

        style = Style()
        style.theme_use('clam')
        style.configure('TCombobox', selectborderwidth=0)
        style.configure('TCombobox', selectbackground="white")
        style.configure('TCombobox', selectforeground='black')
        style.map('TCombobox', fieldbackground=[('readonly', 'white')])
        style.map('TCombobox', selectforeground=[('readonly', 'black')])
        style.map('TCombobox', selectbackground=[('disabled', 'white')])
        style.map('TCombobox', foreground=[('disabled', 'grey')])

        root.mainloop()

    class _Configuration:
        def __init__(self, master, row, service_plugins, deployment_plugins,
                     width=15):
            self._master = master
            self._row = row
            self._service_plugins = service_plugins
            self._deployment_plugins = deployment_plugins

            self._service_widget = Combobox(master)
            self._identity_widget = Entry(master)
            self._cur_pass_widget = Entry(master)
            self._new_pass_widget = Entry(master)
            self._deploy_on_widget = Combobox(master)
            self._status_widget = Label(master)

            self._identity_widget_width = width + 10

            self._supported_services = sorted(
                list(self._service_plugins.keys()),
                key=str.lower
            )

            self._supported_deployment_targets = sorted(
                list(self._deployment_plugins.keys()),
                key=str.lower
            )

            self._service_widget.config(

                width=width,
                justify='center',
                values=['<Nothing>'] + self._supported_services,
                state='readonly',
            )
            self._service_widget.bind(
                '<<ComboboxSelected>>', self.service_selected
            )
            self._identity_widget.config(width=self._identity_widget_width)
            self._cur_pass_widget.config(width=width, show="*")
            self._new_pass_widget.config(width=width, show="*")
            self._deploy_on_widget.config(
                width=width,
                justify='center',
                values=['<Nothing>'] + self._supported_deployment_targets,
                state='readonly'
            )
            self._status_widget.config(
                width=width + 10,
                anchor='center',
            )

            self._service_widget.current(0)
            self._deploy_on_widget.current(0)

            self._service_widget.grid(column=0, row=row, pady=(0, 1))
            self._identity_widget.grid(column=1, row=row, pady=(0, 1))
            self._cur_pass_widget.grid(column=2, row=row, pady=(0, 1))
            self._new_pass_widget.grid(column=3, row=row, pady=(0, 1))
            self._deploy_on_widget.grid(column=4, row=row, pady=(0, 1))
            self._status_widget.grid(column=5, row=row, pady=(0, 1))

        def get_service(self):
            """
            Returns the text of the currently selected service.
            :returns: str -- selected service (e. g. 'Google')
            """
            return self._service_widget.get()

        def get_identity(self):
            """
            Returns the text of the identity entry.
            :returns: str -- text of the identity entry
            """
            return self._identity_widget.get()

        def get_cur_pass(self):
            """
            Returns the text of the passphrase entry.
            :returns: str -- text of the passphrase entry
            """
            return self._cur_pass_widget.get()

        def get_new_pass(self):
            """
            Returns the text of the new passphrase entry.
            :returns: str -- text of the new passphrase entry
            """
            return self._new_pass_widget.get()

        def get_status(self):
            """
            Returns the text of the status label.
            :returns: str -- text of the status label
            """
            return self._status_widget.cget('text')

        def get_deployment_target(self):
            """
            Returns the text of the currently selected deployment target.
            :returns: str -- selected deployment target (e. g. 'Chrome')
            """
            return self._deploy_on_widget.get()

        def set_status(self, text, background):
            """
            Set the text of the status label.
            :param text: Text of the status label.
            :type text: str
            :param background: background supported by tkinter
            :type background: str
            :returns: None
            """
            self._status_widget.config(text=text, background=background)

        def disable_widgets(self):
            """
            Disables all widgets, which can be changed by the user.
            :returns: None
            """
            self._service_widget.config(state='disable')
            self._identity_widget.config(state='disable')
            self._cur_pass_widget.config(state='disable')
            self._new_pass_widget.config(state='disable')
            self._deploy_on_widget.config(state='disable')

        def enable_widgets(self):
            """
            Enables all widgets, which can be changed by the user.
            The service and the _deploy_on_widget are set to readonly instead
            of normal since they should be selectable but their strings
            shouldn't be changeable.
            :returns: None
            """
            self._service_widget.config(state='readonly')
            self._identity_widget.config(state='normal')
            self._cur_pass_widget.config(state='normal')
            self._new_pass_widget.config(state='normal')
            self._deploy_on_widget.config(state='readonly')

        def is_filled(self, check_deploy=False):
            """
            Checks if one configuration is filled and therefore ready for
            change login or deploying. If check_deploy is set, it's checked
            if a deployment target is set.
            :param check_deploy: True if deployment target should be checked
            :type: bool
            :returns: bool -- True if is filled, False if not
            """
            if self.get_service() in self._supported_services and \
                    self.get_identity() and \
                    self.get_cur_pass() and \
                    self.get_new_pass():

                if not check_deploy:
                    return True
                elif self._deploy_on_widget.get() in \
                        self._supported_deployment_targets:
                    return True

            return False

        def service_selected(self, *args):  # pylint: disable=W0612,W0613
            """
            Is called as soon a service is selected and checks if default
            identities are provided by the selected service plugin. If there
            are several default identities provided the _identity_widget is
            replaced by a Combobox.
            :param args: Currently only used internally by tkinter,
            not explicitly.
            :returns: None
            """

            # if '<Nothing>' selected
            if self._service_widget.get() not in self._supported_services:
                self._identity_widget = Entry(
                    self._master,
                    width=self._identity_widget_width,
                )
                self._identity_widget.grid(column=1, row=self._row)
                return

            default_identity = \
                self._service_plugins[self._service_widget.get()][
                    'default_identity']

            self._identity_widget.grid_remove()

            if len(default_identity) == 1:
                del self._identity_widget
                self._identity_widget = Entry(
                    self._master,
                    width=self._identity_widget_width,
                )
                self._identity_widget.insert(0, default_identity[0])
            else:
                del self._identity_widget
                self._identity_widget = Combobox(
                    self._master,
                    width=self._identity_widget_width - 2,
                    values=default_identity
                )
                self._identity_widget.current(0)

            self._identity_widget.grid(column=1, row=self._row)

    class _LoggingArea(Notebook):
        def __init__(self, master, width, height):
            Notebook.__init__(self, master, width=width, height=height)

            self._log_widget = \
                self._LogWidget(master=self)
            self._change_history_widget = \
                self._ChangeHistoryWidget(master=self)

            self.add(self._log_widget, text='Log')
            self.add(self._change_history_widget, text='Change/deploy history')

            # history_table = cht.ChangeHistoryTable(self)
            # history_table.pack(fill='x')

            # self.add(history_table, text='hehe')

            """
            history_table.add_entry("Google", "eins@gmail.com", "altes "
                                                                "passsdfsdfsdfsdfsdfdsfsdfdsf",
                          "neues pass",
                          "Chromium", "SUCCESS")
            """

        class _LogWidget(tk.Frame, logging.Handler):
            def __init__(self, master):
                logging.Handler.__init__(self)

                logging.Handler.__init__(self)
                fmt = '[%(asctime)s] [%(name)s] [%(levelname)s] %(message)s'
                fmt_date = '%T'
                formatter = logging.Formatter(fmt, fmt_date)
                self.setFormatter(formatter)

                tk.Frame.__init__(self, master)

                self.pack(fill="both", expand=True)
                # ensure a consistent GUI size
                self.grid_propagate(False)
                # implement stretchability
                self.grid_rowconfigure(0, weight=1)
                self.grid_columnconfigure(0, weight=1)

                self._log_text = tk.Text(self)
                self._log_text.grid(
                    row=0,
                    column=0,
                    sticky="nsew",
                    padx=2,
                    pady=2
                )

                scrollbar = tk.Scrollbar(self, command=self._log_text.yview)
                scrollbar.grid(row=0, column=1, sticky='nsew')
                self._log_text['yscrollcommand'] = scrollbar.set

            def emit(self, record):
                """Handles log records issued by loggers.

                :param record: log record
                :type record: logging.LogRecord
                :return: None
                """

                # TODO: dirty, magic string
                # Ignore the Change history logger
                # Since this class is assigned to the root logger, it gets all
                # messages of loggers. Since we only want to see passphrases
                # changes in the change history area, we have to reject log
                # records from the "change history" logger.
                if record.name == "Change History":
                    return

                # insert a log entry on top of the text field
                self._log_text.insert("0.0", self.format(record) + "\n")

        class _ChangeHistoryWidget(cht.ChangeHistoryTable):
            def __init__(self, master):
                cht.ChangeHistoryTable.__init__(self, master, bg="white")

            def add_entry(self, service, identity, cur_pass, new_pass,
                          status, deployment_target=None):
                # TODO: deployment_target

                return cht.ChangeHistoryTable.add_entry(
                    self, service, identity, cur_pass, new_pass,
                    status, deployment_target)

        def add_change_history_entry(self, service, identity, cur_pass,
                                     new_pass, status, deployment_target=None):
            return self._change_history_widget.add_entry(
                service, identity, cur_pass, new_pass, status, deployment_target
            )

        def get_log_widget_handler(self):
            """Returns the log widget, which can be used as an
            logging handler, since it inherhits form logging.Handler."""

            return self._log_widget

    def _gui_change_passphrases(self):
        """Spawns threads to execute service plugins for each row which is
        filled with enough information.

        Is called when the "Change logins" button is pressed.

        :return: None
        """

        def execute_service_plugin(configuration):
            """Executes service plugin by calling the change_passphrase
            function with data provided by configuration and sets the status
            label within the GUI appropriately.

            Before execution the text of the status label is set to "CHANGING",
            and after execution it is set to a text dependent on the return
            value of the change_passphrase function.

            :param configuration: Contains information about service and
                                  authentication data.
            :type configuration: _Configuration
            :return: None
            """

            # log credentials change within change history area
            status = self._logging_area.add_change_history_entry(
                configuration.get_service(),
                configuration.get_identity(),
                configuration.get_cur_pass(),
                configuration.get_new_pass(),
                cht.CHTStatus.CHANGING
            )

            configuration.set_status(text="CHANGING", background='yellow')

            ret = self._passdeploy.change_passphrase(
                configuration.get_service(),
                configuration.get_identity(),
                configuration.get_cur_pass(),
                configuration.get_new_pass(),
            )

            if ret == 0:
                configuration.set_status(text="CHANGED", background='green')
                status.set_status(cht.CHTStatus.CHANGED)
            elif ret == 1:
                configuration.set_status(text="NOT CHANGED", background='red')
                status.set_status(cht.CHTStatus.NOT_CHANGED)
            elif ret == 2:
                configuration.set_status(text="NOT SURE IF CHANGED",
                                         background='yellow')
                status.set_status(cht.CHTStatus.NOT_SURE_IF_CHANGED)
            else:
                configuration.set_status(text="ERROR (NOT SURE IF CHANGED)",
                                         background='red')
                status.set_status(cht.CHTStatus.CHANGE_ERROR)

            # we have to enable widgets here, because there are disabled
            # outside and only we know when execution of the plugin is finished
            configuration.enable_widgets()

        for configuration in self._configurations:
            configuration.disable_widgets()

            if configuration.is_filled():
                try:
                    t_exec_service_plugin = Thread(
                        target=execute_service_plugin,
                        args=(configuration,)
                    )
                    t_exec_service_plugin.start()
                except BaseException:
                    logging.exception(
                        'Unable to start thread for '
                        + configuration.get_service() + ' service plugin.'
                    )
            else:
                configuration.enable_widgets()

        return

    def _gui_deploy_changes(self):
        """Is called when the "Deploy" button is pressed and deploys
        passphrases entered within the GUI.

        For each properly filled row (no matter if the passphrase has been
        changed before) in the GUI a thread is spawned to execute the
        selected deployment plugin to deploy passphrases concurrently.

        :return: None
        """

        def execute_deployment_plugin(configuration):
            """Execute deployment plugin by calling the deploy function with
            data provided by configuration and sets the status label within
            the GUI appropriately.

            Before execution the text of the status label is set to "DEPLOYING",
            and after execution it is set to a text dependent on the return
            value of the deploy function.

            :param configuration: Contains information about service,
                                  authentication data and deployment target.
            :type configuration: _Configuration
            :return: None
            """

            # log deployment within the change history area
            status = self._logging_area.add_change_history_entry(
                configuration.get_service(),
                configuration.get_identity(),
                configuration.get_cur_pass(),
                configuration.get_new_pass(),
                cht.CHTStatus.DEPLOYING,
                configuration.get_deployment_target()
            )

            configuration.set_status(text="DEPLOYING", background='yellow')

            ret = self._passdeploy.deploy_passphrase(
                configuration.get_service(),
                configuration.get_identity(),
                configuration.get_cur_pass(),
                configuration.get_new_pass(),
                configuration.get_deployment_target()
            )

            if ret == 0:
                configuration.set_status(text="DEPLOYED", background='green')
                status.set_status(cht.CHTStatus.DEPLOYED)
            elif ret == 1:
                configuration.set_status(text="NOT DEPLOYED", background='red')
                status.set_status(cht.CHTStatus.NOT_DEPLOYED)
            elif ret == 2:
                configuration.set_status(text="NOT SURE IF DEPLOYED",
                                         background='yellow')
                status.set_status(cht.CHTStatus.NOT_SURE_IF_DEPLOYED)
            else:
                configuration.set_status(text="ERROR (NOT SURE IF DEPLOYED)",
                                         background='red')
                status.set_status(cht.CHTStatus.DEPLOY_ERROR)

            # we have to enable widgets here, because there are disabled
            # outside and only we know when execution of the plugin is finished
            configuration.enable_widgets()

        for configuration in self._configurations:
            configuration.disable_widgets()

            if configuration.is_filled(check_deploy=True):
                try:
                    t_exec_deployment_plugin = Thread(
                        target=execute_deployment_plugin,
                        args=(configuration,)
                    )
                    t_exec_deployment_plugin.start()
                except BaseException:
                    logging.exception(
                        'Unable to start thread for '
                        + configuration.get_deployment_target() +
                        'deployment plugin.'
                    )

            configuration.enable_widgets()
