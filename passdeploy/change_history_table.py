# coding=utf-8

try:
    import tkinter as tk
except ImportError:
    import Tkinter as tk

import threading
import platform


class CHTStatus:
    CHANGING, \
    DEPLOYING, \
    CHANGED, \
    DEPLOYED, \
    NOT_CHANGED, \
    NOT_DEPLOYED, \
    NOT_SURE_IF_CHANGED, \
    NOT_SURE_IF_DEPLOYED, \
    CHANGE_ERROR, \
    DEPLOY_ERROR = range(10)

    def __init__(self, status_widget):
        """

        :param status_widget: 
        :type status_widget: tk.Widget
        """
        self._status_widget = status_widget

    def set_status(self, status):
        if status == CHTStatus.CHANGING:
            self._status_widget.config(text="CHANGING", bg="yellow")
        elif status == CHTStatus.DEPLOYING:
            self._status_widget.config(text="DEPLOYING", bg="yellow")
        elif status == CHTStatus.CHANGED:
            self._status_widget.config(text="CHANGED", bg="green")
        elif status == CHTStatus.DEPLOYED:
            self._status_widget.config(text="DEPLOYED", bg="green")
        elif status == CHTStatus.NOT_CHANGED:
            self._status_widget.config(text="NOT CHANGED", bg="red")
        elif status == CHTStatus.NOT_DEPLOYED:
            self._status_widget.config(text="NOT DEPLOYED", bg="red")
        elif status == CHTStatus.NOT_SURE_IF_CHANGED:
            self._status_widget.config(text="NOT SURE IF CHANGED", bg="orange")
        elif status == CHTStatus.NOT_SURE_IF_DEPLOYED:
            self._status_widget.config(text="NOT SURE IF CHANGED", bg="orange")
        elif status == CHTStatus.CHANGE_ERROR:
            self._status_widget.config(text="ERROR (NOT SURE IF CHANGED)",
                                       bg="red")
        elif status == CHTStatus.DEPLOY_ERROR:
            self._status_widget.config(text="ERROR (NOT SURE IF DEPLOYED)",
                                       bg="red")
        else:
            raise Exception("Unknown status parameter.")


class ChangeHistoryTable(tk.Frame):
    def __init__(self, master=None, **kw):
        tk.Frame.__init__(self, master, kw)
        self.pack(fill="both", expand=True)

        self._counter = 1

        self._lock = threading.Lock()

        base_width = 15
        self._deployment_target_width = base_width
        self._service_width = base_width
        self._identity_width = base_width + 5
        self._old_pass_width = base_width
        self._new_pass_width = base_width
        self._status_width = base_width + 20
        self._counter_width = 5

        head_row = tk.Frame(self, bg="white")
        head_row.pack(fill='x')

        self._body_frame = VerticalScrolledFrame(self, bg="white")

        self._body_frame.pack(fill='x')

        counter = tk.Label(head_row, text="#")
        deployment_target = tk.Label(head_row, text="Deployment target")
        service = tk.Label(head_row, text="Service")
        identity = tk.Label(head_row, text="Identity")
        old_pass = tk.Label(head_row, text="Old passphrase")
        new_pass = tk.Label(head_row, text="New passphrase")
        status = tk.Label(head_row, text="Status")

        counter.config(width=self._counter_width)
        deployment_target.config(width=self._deployment_target_width)
        service.config(width=self._service_width)
        identity.config(width=self._identity_width)
        old_pass.config(width=self._old_pass_width)
        new_pass.config(width=self._new_pass_width)
        status.config(width=self._status_width)

        for widget in (counter, deployment_target, service, identity, old_pass,
                       new_pass, status):
            widget.config(highlightbackground="black", highlightthickness=1)
            widget.pack(side=tk.LEFT)

    def add_entry(self, service, identity, old_pass, new_pass,
                  status, deployment_target=None):

        def _on_over_pass_entry(event):
            event.widget.config(show="")

        def _on_leave_pass_entry(event):
            event.widget.config(show="*")

        def _on_over_row(event):
            event.widget.config(highlightbackground="black")

        def _on_leave_row(event):
            event.widget.config(highlightbackground="white")

        with self._lock:
            counter = self._counter
            self._counter += 1

        row = tk.Frame(self._body_frame.interior, bg="white", bd=0,
                       highlightthickness=1, highlightbackground="white")
        row.pack(side=tk.BOTTOM, fill='x', pady=(1, 0), padx=(0, 1))

        w_counter = tk.Label(row, text=counter)
        w_deployment_target = tk.Label(row, text=deployment_target)
        w_service = tk.Label(row, text=service)
        w_identity = tk.Entry(row, textvariable=tk.StringVar(
            value=identity))
        w_old_pass = tk.Entry(row, show="*", textvariable=tk.StringVar(
            value=old_pass))
        w_new_pass = tk.Entry(row, show="*", textvariable=tk.StringVar(
            value=new_pass))
        w_status = tk.Label(row)

        w_counter.config(width=self._counter_width)
        w_deployment_target.config(width=self._deployment_target_width)
        w_service.config(width=self._service_width)
        w_identity.config(width=self._identity_width, takefocus=0, bd=0,
                          state='readonly')
        w_old_pass.config(width=self._old_pass_width, takefocus=0, bd=0,
                          state='readonly')
        w_new_pass.config(width=self._new_pass_width, takefocus=0, bd=0,
                          state='readonly')
        w_status.config(width=self._status_width)

        w_old_pass.bind("<Enter>", _on_over_pass_entry)
        w_new_pass.bind("<Enter>", _on_over_pass_entry)
        row.bind("<Enter>", _on_over_row)
        w_old_pass.bind("<Leave>", _on_leave_pass_entry)
        w_new_pass.bind("<Leave>", _on_leave_pass_entry)
        row.bind("<Leave>", _on_leave_row)

        for widget in (w_counter, w_deployment_target, w_service, w_identity,
                       w_old_pass, w_new_pass, w_status):
            widget.config(bg="white")
            widget.pack(side=tk.LEFT, padx=(0, 2))

        w_status.pack(padx=(0, 0))

        # set given status value for status widget
        # assumption: background color change is only possible after pack()
        CHTStatus(w_status).set_status(status)

        w_identity.config(readonlybackground="white")
        w_old_pass.config(readonlybackground="white")
        w_new_pass.config(readonlybackground="white")

        return CHTStatus(w_status)


class VerticalScrolledFrame(tk.Frame):
    """A pure Tkinter scrollable frame that actually works!

    * Use the 'interior' attribute to place widgets inside the scrollable frame
    * Construct and pack/place/grid normally
    * This frame only allows vertical scrolling

    Based on: http://tkinter.unpythonic.net/wiki/VerticalScrolledFrame
    """

    def __init__(self, parent, *args, **kw):
        tk.Frame.__init__(self, parent, *args, **kw)

        # create a canvas object and a vertical scrollbar for scrolling it
        vscrollbar = tk.Scrollbar(self, orient=tk.VERTICAL)
        vscrollbar.pack(fill=tk.Y, side=tk.RIGHT, expand=tk.FALSE)
        canvas = self._canvas = tk.Canvas(self, bd=0, highlightthickness=0,
                                          yscrollcommand=vscrollbar.set,
                                          bg="white")
        canvas.pack(side=tk.LEFT, fill=tk.BOTH, expand=tk.TRUE)
        vscrollbar.config(command=canvas.yview)

        # reset the view
        canvas.xview_moveto(0)
        canvas.yview_moveto(0)

        # create a frame inside the canvas which will be scrolled with it
        interior_default_width = 130
        self.interior = interior = tk.Frame(canvas,
                                            bg="white",
                                            height=interior_default_width)
        interior.pack(fill=tk.BOTH, expand=tk.TRUE)
        interior_id = canvas.create_window(0, 0, window=interior, anchor=tk.NW)

        # track changes to the canvas and frame width and sync them,
        # also updating the scrollbar
        def _configure_interior(event):
            # update the scrollbars to match the size of the inner frame

            if interior.winfo_reqheight() > interior_default_width:
                size = (interior.winfo_reqwidth(), interior.winfo_reqheight())
                canvas.config(scrollregion="0 0 %s %s" % size)
            else:
                canvas.config(scrollregion="0 0 0 %s" % interior_default_width)

        interior.bind('<Configure>', _configure_interior)

        self._bind_to_mousewheel(interior)

        def _configure_canvas(event):
            canvas.itemconfigure(interior_id, width=event.width)

        canvas.bind('<Configure>', _configure_canvas)

        return

    def _bind_to_mousewheel(self, widget):
        if platform.system() == "Linux":
            widget.bind_all('<4>', self._on_mousewheel)
            widget.bind_all('<5>', self._on_mousewheel)
        else:
            # Windows and MacOS
            widget.bind_all("<MouseWheel>", self._on_mousewheel)

    def _on_mousewheel(self, event):
        mousewheel_speed = 2  # TODO: dirty magic number

        self._canvas.yview_scroll(int(-1 * (event.delta / 120)), "units")

        if platform.system() == 'Linux':
            if event.num == 4:
                self._canvas.yview_scroll((-1) * mousewheel_speed, "units")
            elif event.num == 5:
                self._canvas.yview_scroll(mousewheel_speed, "units")
        elif platform.system() == 'Windows':
            self._canvas.yview_scroll((-1) * int((event.delta / 120) *
                                                 mousewheel_speed), "units")
        elif platform.system() == 'Darwin':
            self._canvas.yview_scroll(event.delta, "units")
