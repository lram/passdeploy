* Create a simple intermediate language for writing plugins so that plugin 
files contain only absolute necessary code lines and are more easier to audit.
  * Once loaded, the plugin files could be translated to Python files.