* unsolved problems
  * check how to handle two-factors logins
  * Javascript-only pages
    * Play with Python OAuth libraries to handle websites, which are using 
      OAuth for login processes (e. g. https://pypi.python.org/pypi/oauthlib, 
      requests oauth features)
    * or maybe try using Selenium (http://selenium-python.readthedocs.io/ , 
   http://www.seleniumhq.org/)

* core
  * add timeout for plugin threads
  * management for backup files
  * cancel functionality
  * wildcards for origin_urls
  * add functionality for domain unspecific but web service specific plugins
    * create a platform which supports plugins which are handling passphrase 
    changing of common web applications (e. g. Wordpress, Simple Machines 
    Forum, osCommerce, ...)
      * careful design decisions on core and (G)UI have to be made
  
* plugins
    * provide platform information in plugins
    * change `deploy` function specification and change parts of passdeploy's 
      source. Actually only `new_pass` is necessary, the rest is optional
    * Firefox deployment plugin
      * add opportunity to choose profiles
    * Chrome deployment plugin
      * create backup files
      * more detailed debug messages on SQL operations (to understand, why 
        there was not change in the database)
    * additional plugins
      * Apple Mail
      * Thunderbird/Icedove
      * Pidgin


* scraplib
    * provide functions for scraplib to handle CAPTCHAs easier
      * should be supported by (G)UI

* GUI
  * button to show passphrase
  * add passphrase generator
  * add opportunity to add more logins
  * menu
    * option to switch to DEBUG level

* misc
  * create RATIONALE file
  * threat modeling
  * check how passdeploy to easily install on Windows
  * enforce coding style over all python files in project
    * git pre-commit checks?
  * add notes to make backups in deployment specification
  * release versioning
  * https://pypi.python.org/pypi/coverage
  * create screenshots for the git repository
  * move some TODOs to IDEAs file.

* testing
  * check how passdeploy behaves on Windows
  * test deployment plugins on Windows and Mac
  * setup testing infrastructure
    * DONE: web service plugins testing script
