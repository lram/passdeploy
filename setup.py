#!/usr/bin/env python
# coding=utf-8
from setuptools import setup, find_packages

setup(
    name='passdeploy',
    version='0.0.1b0',
    packages=find_packages(),
    py_modules=["scraplib"],
    url='https://gitlab.spline.inf.fu-berlin.de/lram/passdeploy',
    license='GPL',
    author='Marl Joos',
    author_email='mail@m-a-r-l.de',
    description='Passphrase change and deployment framework.',

    install_requires=[
        'requests',
        'bs4',
        'urllib3',
        'six',
        'lxml'
    ],

    entry_points={
        'console_scripts': ['passdeploy=passdeploy.passdeploy_main:main'],
    }
)
