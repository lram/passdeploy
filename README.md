# passdeploy

passdeploy is a passphrase changing and deployment framework written in
Python,
which provides a simple GUI and a plugin system to automate passphrase
changing and deployment processes.
passdeploy also includes a library (scraplib.py) which makes easier to write
plugins for changing passphrases for webservices.

passdeploy already includes 5 service plugins and 1 deployment plugins to
easily change passphrases for Google, Facebook and private SSH keys or to
deploy your passphrases into Chromium.

For more information take a look at the docs directory.

## Installation

If you want to install passdeploy as a local user without admin rights just 
install its dependencies within the passdeploy directory:
```
pip install --user -r requirements.txt
```

If you want to install passdeploy globally and you have admin rights:
```
sudo pip install ./passdeploy
```

## Run

If you installed passdeploy globally, you can just start it by calling:

```
passdeploy
```

### Unix-like (GNU/Linux, mac OS)

If you installed passdeploy locally just execute the `start.sh` file within 
the passdeploy directory:
```
./start.sh
```

## Documentation
Take a look at the [docs](/docs/) directory.

See:
* [Service plugin specification](/docs/service-plugin-specification.md), to 
know, how to write service plugins.
  * [service plugin template](/docs/service-plugin-template/main.py)
* [Deployment plugin specification](/docs/deployment-plugin-specification.md)
, to know how to write deployment plugins.
  * [deployment plugin template](/docs/deployment-plugin-template/main.py)
* [Writing service plugins for web services](/docs/writing-web-service-plugins.md),
to get help for writing service plugins for web services as easy and 
efficient as possible.

## Requirements
[0] http://docs.python-requests.org

[1] https://www.crummy.com/software/BeautifulSoup/

[2] https://urllib3.readthedocs.io/
