#!/usr/bin/env python3
# coding=utf-8

import logging

logger = logging.getLogger(__name__)
# logging.basicConfig(level=logging.DEBUG)


def deploy(origin_urls, identity, old_pass, new_pass):
    # put your deployment code here
    deployment_status = 0

    if 0 == deployment_status:
        # success
        return 0
    elif 1 == deployment_status:
        # fail
        logger.warning("Deployment failed because of XYZ.")
        return 1
    else:
        # not sure if deployment was successful or not
        logging.warning("Deployment canceled/error while XYZ.")
        return 2


# if you want to execute this as a stand-alone script
if __name__ == '__main__':
    # args_parser()
    deploy(['origin_url1'],
           'test@gmail.com',
           'old-passphrase',
           'new-passphrase'
           )
