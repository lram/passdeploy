# Deployment plugin specification (v0.1)

Deployment plugins take the identity, old passphrase, the new 
passphrase and some information from the currently selected service plugin to
 (mostly locally) deploy the passphrase to the current system.

Deployment targets can for example be password managers of browser, email 
client or instant messaging client. Since the plugins are written in Python, 
you can define your deployment process however you want, as long you follow 
specific conventions.

Deployment plugins are located in *plugins/services/$PLUGIN_NAME/* and are 
always named *main.py*.
 
A deployment plugin simply has to implement the `deploy` function and return 
values which conform to the convention. 

## deploy(origin_urls, identity, old_pass, new_pass)

`depoy` takes `origin_urls`, `identity`, `old_pass`, `new_pass` and deploys 
the new passphrase.

* `origin_urls` (type: list of str): List of URLs, which are related to the 
selected service. E. g. URLs of login forms.
* `identity` (type: str): Represents the identity (e. g. username).
* `old_pass` (type: str): Represents the old/current passphrase, which should 
be replaced.
* `new_pass` (type: str): Represents the new passphrase, which should be 
deployed and probably replaces the old/current passphrase.

Return values:
* `0`, if the deployment was successful.
* `1`, if the deployment was not successful.
* `2`, if there is uncertainty about the state of the deployment.

Uncertainty should obviously be avoided as much as possible, but may not be 
possible always.
 
Don't raise exceptions and handle exceptions at the plugin level. passdeploy 
catches exceptions, returns an error to the user and assumes uncertainty 
about the deployment.

## Deployment plugin template

The template can also be found in the
[deployment plugin template folder](/deployment-plugin-template/).

```python
#!/usr/bin/env python3
# coding=utf-8

import logging

logger = logging.getLogger(__name__)
# logging.basicConfig(level=logging.DEBUG)


def deploy(origin_urls, identity, old_pass, new_pass):
    # put your deployment code here
    deployment_status = 0
    
    if 0 == deployment_status:
        # success
        return 0
    elif 1 == deployment_status:
        # fail
        logger.warning("Deployment failed because of XYZ.")
        return 1
    else:
        # not sure if deployment was successful or not
        logging.warning("Deployment canceled/error while XYZ.")
        return 2
        
        
# if you want to execute this as a stand-alone script
if __name__ == '__main__':
    # args_parser()
    deploy(
        ['origin_url1'],
        'test@gmail.com',
        'old-passphrase',
        'new-passphrase'
    )


```
