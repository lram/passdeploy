#!/usr/bin/env python3
# coding=utf-8

import logging

logger = logging.getLogger(__name__)
# logging.basicConfig(level=logging.DEBUG)

origin_urls = [
    'https://en.wikipedia.org/w/index.php',
    'https://de.wikipedia.org/w/index.php'
]


def init_as_plugin():
    # put your initialization code here
    init_success = False

    if init_success:
        # successful initialization and return default values
        default_value = {
            'default_identity': [
                'admin', 'administrator'
            ]
        }

        return default_value
        # you can simply return 0 if you don't want to return default values
        # return 0
    else:
        # something went wrong
        logger.warning("Error with XYZ while initializing.")

        return 2


def change_passphrase(identity, cur_pass, new_pass):
    # put your passphrase changing code here
    change_successful = 0

    if 0 == change_successful:
        # successful
        return 0
    elif 1 == change_successful:
        logger.warning("Wrong passphrase.")
        return 0
    else:
        # unexpected error
        # don't know whether passphrase was changed or not
        logger.warning("Unexpected condition while doing XYZ.")
        return 2


# if you want to execute this as a stand-alone script
if __name__ == "__main__":
    change_passphrase(
        'username',
        'current-passphrase',
        'new-passphrase'
    )
