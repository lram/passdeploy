
# Service plugin specification (v0.1)

Service plugins aim at making passphrase changing 
processes easy for a specific service.
A service can be a web service such as Wikipedia or Google, but can also be 
be a local service such as private SSH keys. Since the plugins are written in
Python, you can define your passphrase changing process however you want, as 
long you follow specific conventions.

Service plugins are located in *plugins/services/$PLUGIN_NAME/* and are 
always named *main.py*.
 
A service plugin contains the following variables and functions:
* **logger object (optional)**: to provide logging messages
* **origin_urls (optional, type: list of strings)**: to provide location 
information
 to deployment 
plugins
* **init_as_plugin() (optional)**: to initialize the plugin and return default 
values to the GUI
* **change_passphrase(identity, cur_pass, new_pass)**: to execute the actual 
passphrase changing process

## logger object (optional)
Although this is optional it is highly 
recommended to use one logger object to inform users about the current state 
of the passphrase changing process or why a passphrase changing process 
actually has failed.
Just include the logging package and create a module-level logger:

```
import logging
logger = logging.getLogger(__name__)
#logging.basicConfig(level=logging.DEBUG)
```
and use the logging functions, which fits best:
```
logger.debug("Debug. Only show me if level=logging.DEBUG)
logger.warning("Passphrase couldn't be change since the passphrase was wrong")
logger.error("Unexpected error while doing XYZ.")
```
For more information about logging in Python 3, see
https://docs.python.org/3/howto/logging.html

## origin_urls (optional)

`origin_urls` is a list of strings, which contains URLs of the service (e. g. 
URLs of the login form). 
This information is needed by deployment plugins to change the correct 
passphrase.
 
Example:
```
origin_urls = [ 
    'https://en.wikipedia.org/w/index.php',
    'https://de.wikipedia.org/w/index.php'
]
```

## init_as_plugin() (optional)

`init_as_plugin` initializes the plugin and returns an integer or a 
dictionary depended on if default values should be returned.

Return values:
* `0`, if the initialization was successful
* `1`, if the initialization was unsuccessful (the plugin won't be 
loaded)
* `{ 'default_identity': [ 'id1', 'id2' ] }` if the initialization was 
successful and `id1` and `id2` should be provided as default values for 
the identity.

## change_passphrase(identity, cur_pass, new_pass)

`change_passphrase` takes the identity, the current passphrase as well as the
 new passphrase to change it at the service.

* `identity` (type: str): Represents the identity (e. g. username, path to 
private key).
* `cur_pass` (type: str): Represents the current passphrase.
* `new_pass` (type: str): Represents the new passphrase.

Return values:
* `0`, if the passphrase change was successful.
* `1`, if the passphrase couldn't be changed.
* `2`, if there is uncertainty about the state of the passphrase change.

Uncertainty should obviously be avoided as much as possible, but isn't always
 possible when handling with unclean APIs (e. g. passphrase changing via web 
 scraping).
 
Don't raise exceptions and handle exceptions at the plugin level. passdeploy 
catches exceptions, returns an error to the user and assumes uncertainty 
about the passphrase changing process.

## Service plugin template

The template can also be found in the
[service plugin template folder](/service-plugin-template/).

```python
#!/usr/bin/env python3
# coding=utf-8

import logging

logger = logging.getLogger(__name__)
# logging.basicConfig(level=logging.DEBUG)

origin_urls = [ 
    'https://en.wikipedia.org/w/index.php',
    'https://de.wikipedia.org/w/index.php'
]


def init_as_plugin():
    # put your initialization code here
    init_success = False
    
    if init_success:
        # successful initialization and return default values
        default_value = { 
            'default_identity': [
                'admin', 'administrator' 
            ]
        }
        
        return default_value
        # you can simply return 0 if you don't want to return default values
        # return 0
    else:
        # something went wrong
        logger.warning("Error with XYZ while initializing.")
        
        return 2
    
    
def change_passphrase(identity, cur_pass, new_pass):
    # put your passphrase changing code here
    change_successful = 0
    
    if 0 == change_successful:
        # successful
        return 0
    elif 1 == change_successful:
        logger.warning("Wrong passphrase.")
        return 0
    else:
        # unexpected error
        # don't know whether passphrase was changed or not
        logger.warning("Unexpected condition while doing XYZ.")
        return 2
        
        
# if you want to execute this as a stand-alone script
if __name__ == "__main__":
    change_passphrase(
        'username',
        'current-passphrase',
        'new-passphrase'
    )
    
```
